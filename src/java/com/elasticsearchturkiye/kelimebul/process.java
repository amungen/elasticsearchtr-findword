/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elasticsearchturkiye.kelimebul;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author ahmet.mungen
 */
public class process {

    String urlserver1 = "http://127.0.0.1:9200/";

    public static void main(String[] args) {
        try {

            process is = new process();
            //elasticsearch'i formatlıyoruz bu işlem zaman aldığı için bu işlemi çalıştırdıkdan 10 sn sonraya kadar manuel olarak beklemek gerekir
            //   is.format("words/kufur");
            //5 adet txt dosyamızı ayrı ayrı elasticsearch'e yüklüyoruz
            //bu işlemi 1 kere yapmamız yeterli. Elasticsearch'e her seferinde gönderirsek 2 kere listeler. 1 kere çalıştırdıktan sonra yorum satırına dönüştürün
            for (int i = 0; i < 5; i++) {
                String path = "C:\\Users\\ahmet\\deneme" + i + ".txt";
                System.out.println("path = " + path);
                File f = new File(path);
                Scanner sc = new Scanner(f);
                String text = "";
                while (sc.hasNext()) {
                    text = text + sc.nextLine();
                }
                // System.out.println("text = " + text);
                is.add("dosya" + i, text);
            }

            //Küfür listemizi elassticsearch içinde arayıp küfür içeren dosya nosunu bulmaya çalışıyoruz
            String[] kufurlist = {"Flight", "light", "test", "test", "gathered"};
            for (int j = 0; j < kufurlist.length; j++) {
                String kufurtemp = kufurlist[j];
                filemodel[] sonuclar = is.Search(kufurtemp);
                for (int i = 0; i < sonuclar.length; i++) {
                    filemodel sonuclar1 = sonuclar[i];
                    System.out.println("Şu Dosyada: " + sonuclar1.getDosyaismi() + " Şu küfür: " + kufurtemp + " Bulundu.");
                }
                System.out.println("");
            }

        } catch (Exception e) {
        }

    }

    public void add(String dosyaisim, String text) {
        try {
            JSONObject cred = new JSONObject();
            JSONObject auth = new JSONObject();
            JSONObject parent = new JSONObject();
            cred.put("dosyaismi", dosyaisim);
            cred.put("text", text);
            System.out.println("parent. = " + cred.toString());
            String category = "/words/kufur/";
            parent = cred;
            System.out.println("parent = " + parent);
            String url = urlserver1 + category;
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
            wr.write(parent.toString());
            wr.flush();
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            HttpResult = HttpResult - 1;
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                    System.out.println("sb = " + sb);
                }
                br.close();
            } else {
                System.out.println("Http eLSESonuc:" + con.getResponseMessage());
            }

        } catch (Exception e) {
            System.out.println("Error Elastic Yedi");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public filemodel[] Search(String keyword) {
        String JSONTEXT = SearchFirst(keyword);

        org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
        filemodel[] articleresult = null;
        try {
            Object obj = parser.parse(JSONTEXT);
            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
            obj = jsonObject.get("hits");
            jsonObject = (org.json.simple.JSONObject) obj;
            org.json.simple.JSONArray msg = (org.json.simple.JSONArray) jsonObject.get("hits");
            Iterator<org.json.simple.JSONObject> iterator = msg.iterator();
            articleresult = new filemodel[msg.size()];
            int sayac = 0;
            while (iterator.hasNext()) {
                try {
                    org.json.simple.JSONObject jsonObject2 = iterator.next();
                    obj = jsonObject2.get("_source");
                    jsonObject2 = (org.json.simple.JSONObject) obj;
                    String text = jsonObject2.get("text").toString();
                    String dosyaismi = jsonObject2.get("dosyaismi").toString();
                    filemodel a = new filemodel(dosyaismi, text);
                    articleresult[sayac++] = a;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return articleresult;
    }

    public String SearchFirst(String keyword) {
        org.json.simple.JSONObject cred = new org.json.simple.JSONObject();
        org.json.simple.JSONObject auth = new org.json.simple.JSONObject();
        org.json.simple.JSONObject parent = new org.json.simple.JSONObject();
        cred.put("query", keyword);
        auth.put("query_string", cred);
        parent.put("query", auth);
        parent.put("from", "0");
        parent.put("size", "1000");
        String category = "/words/_search"; //or movies/movie/_search
        StringBuilder sb = new StringBuilder();
        try {
            String url = urlserver1 + category;
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
            wr.write(parent.toString());
            wr.flush();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            } else {
                System.out.println("search Response " + con.getResponseMessage());

            }
        } catch (Exception e) {

        }
        return sb.toString();
    }

    public void format(String category) {
        //example parameter          words/kufur
        try {
            String url = urlserver1 + category;
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("DELETE");

//display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                System.out.println("" + sb.toString());

            } else {
                System.out.println(con.getResponseMessage());

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

}
