/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elasticsearchturkiye.kelimebul;


/**
 *
 * @author ahmet
 */
public class filemodel {
  
   private String dosyaismi;
    private String text;
    

    public filemodel() {
    }

    public filemodel(String dosyaismi, String text) {
        this.dosyaismi = dosyaismi;
        this.text = text;
    }

    public String getDosyaismi() {
        return dosyaismi;
    }

    public String getText() {
        return text;
    }

    public void setDosyaismi(String dosyaismi) {
        this.dosyaismi = dosyaismi;
    }

    public void setText(String text) {
        this.text = text;
    }
    
           
}
